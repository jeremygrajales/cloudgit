<?php

// $name = $argv[1];

function createContainers($name) {
    $domain = "gitforidex.com";
    
    // create droppy
    $command = "sudo docker run -d -p 8989 -v /userfiles/" . $name . ":/files -v " . __DIR__ . "/config.json:/root/.droppy/config/config.json --name " . $name . "_droppy --env VIRTUAL_HOST=". $name .".files." . $domain . " --network proxy_gitnet silverwind/droppy";
    print_r('Running command: ' . $command . '\n');
    exec($command);
    
    // create gritty
    $command = "sudo docker run -d -p 1337 -v /userfiles/" . $name . ":/workspace --name " . $name . "_gritty --env VIRTUAL_HOST=".$name.".console." . $domain . " --network proxy_gitnet gritty";
    print_r('Running command: ' . $command . '\n');
    exec($command);
    
    // create app
    // $command = "docker run -d -p 80 --name " . $name . "_cloudgit --env VIRTUAL_HOST=git.".$name."." . $domain . " --network proxy_gitnet cloudgit";
    // exec($command);
}


function removeContainers($name) {
    // rm droppy
    $command = "sudo docker rm -f " . $name . "_droppy";
    exec($command);
    
    // rm gritty
    $command = "sudo docker rm -f " . $name . "_gritty";
    exec($command);
}

?>