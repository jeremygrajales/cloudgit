<?php
$name = $_POST['uid'];

if(!isRunning($name, 'gritty') && !isRunning($name, 'droppy')) {
    include "scripts/containers.php";
    removeContainers($name);
    createContainers($name);
    echo 'build requested';
}
else {
    echo 'These containers already exist!';
}

function isRunning($name, $ext) {
    $ret = exec('sudo docker inspect -f {{.State.Running}} '. $name . '_' . $ext);
    return $ret == 'true'; 
}

?>