<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

require_once __DIR__ .'/vendor/autoload.php';
$loader = new Twig_Loader_Filesystem(__DIR__.'/templates');
$twig = new Twig_Environment($loader, array());

function url($url) {
    return $url == parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
}

if(url('/building')) {
    $uid = $_SESSION['uid'];
    view('building', array('uid' => $uid));
}
else if(url('/register')) {
    if($_SERVER['REQUEST_METHOD'] == "POST") {
        // get uid if available
        if(isset($_POST['email'])) {
            $email = $_POST['email'];
            $uid = substr(md5($email), 0, 8);
            $_SESSION['uid'] = $uid;
            $_SESSION['email'] = $_POST['email'];
            header('Location: /building');
        }
    }
    else if(isset($_SESSION['uid'])) {
        header('Location: /');
    }
    else if($_SERVER['REQUEST_METHOD'] == "GET") {
        view('register');  
    }
}
else if(url('/')) {
    if(!isset($_SESSION['uid'])) {
        header('Location: /login');
    }
    
    else {
        $uid = $_SESSION['uid'];
        $sha = sha1($_SESSION['email']);
        view('index', array('uid' => $uid, 'shaid' => $sha));
    }
}

else if(url('/logout')) {
    session_destroy();
    header('Location: /login?loggedout');
}

else if(url('/login')) {
    $messages = [];
    
    if(isset($_GET['loggedout'])) {
        $messages[] = [
            'type' => 'success',
            'message' => 'You have been logged out'
        ];
    }
    
    // get uid if available
    if(isset($_POST['email'])) {
        $email = $_POST['email'];
        $uid = substr(md5($email), 0, 8);
        $_SESSION['uid'] = $uid;
        $_SESSION['email'] = $_POST['email'];
    }
    
    if(isset($_SESSION['uid'])) {
        header("Location: http://git.gitforidex.com");
    }
    
    else {
        view('login', array('messages' => $messages));
    }
}


function view($template = 'index', $args = array()) {
    global $twig;
    $template = $twig->load($template . '.html');
    echo $template->render($args);
}